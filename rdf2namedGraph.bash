#! /usr/bin/env bash

if [ $# -ne "4"-a $# -ne "6" ]  # Script invoked with bad number of args
then
  echo "Usage: `basename $0` options (-iog)"
  exit 1        # Exit and explain usage, if no argument(s) given.
fi  
# Usage: scriptname -options
# Note: dash (-) necessary

rdfFilePath=""
pictureFilePath=""

while getopts ":i:o:g:" Option
do
  case $Option in
    i     ) echo "Scenario #1: option -i- ${OPTARG}  [OPTIND=${OPTIND}]"; rdfFilePath=${OPTARG};;
    o     ) echo "Scenario #3: option -o- ${OPTARG}  [OPTIND=${OPTIND}]"; trigFilePath=${OPTARG};;
    g     ) echo "Scenario #5: option -g- ${OPTARG}  [OPTIND=${OPTIND}]"; graphURI=${OPTARG};;
    \? ) echo "Usage: $0 -i <RDF file path> [-o <named graph file path>] -g <graph URI>";;
  esac
done

shift $(($OPTIND - 1))
#  Decrements the argument pointer so it points to next argument.
#  $1 now references the first non option item supplied on the command line
#+ if one exists.

# rdfFileType variable is not needed but might be useful later on
fileExtension=${rdfFilePath##*.}
case ${fileExtension} in
  owl  ) rdfFileType="rdfxml";;
  ttl   ) rdfFileType="turtle";;
  trig  ) rdfFileType="trig";;
  \?    ) echo "unknown RDF file type"; exit 2 ;;
esac


if [[ ${trigFilePath} == "" ]]
then
  trigFilePath="${rdfFilePath%.*}.trig"
fi

# trigFileType variable is not needed but might be useful later on
trigFileType=${trigFilePath##*.}


${JENA_HOME}/bin/tdb2.tdbloader --loc /tmp/tdb --graph ${graphURI} ${rdfFilePath} && ${JENA_HOME}/bin/tdb2.tdbdump --output=trig --loc /tmp/tdb > ${trigFilePath} && rm -r /tmp/tdb
