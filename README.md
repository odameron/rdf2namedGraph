# rdf2namedGraph

Utility script for converting a (simple) RDF file into an RDF named graph.


# Dependencies

- [Apache jena](https://jena.apache.org/)
    - Set the `JENA_HOME` environment variable (`export JENA_HOME=/path/to/jenaDirectory`)


# Usage

```bash
rdf2namedGraph.bash -i rdfFilePath [-o trigFilePath] -g graphIdentifier
```


# Todo
- [ ] also generate a VoID description
